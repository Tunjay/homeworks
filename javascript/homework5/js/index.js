"use strict";

function createNewUser() {
    let name = prompt("Your name");
    while (!isNaN(name) || name.trim() === "" || name === "") {
        name = prompt("Type your name, please");
    };

    let surName = prompt("your surname");
    while (!isNaN(surName) || surName === "") {
        surName = prompt("Type your surname, please");
    };

    let birthday = prompt("Birthday", 'dd.mm.yyyy');
    // while (isNaN(birthday) || birthday.trim() === "") {
    //     birthday = prompt("Type your birthday, please");
    // }; 
    // не нашел как писать проверку чтобы принимал только цифры и точу.

    const newUser = {
        firstName:name,
        lastName:surName,
        birthday: birthday,
        // login: this.getLogin(),
        
        getLogin(){
          return (newUser.firstName[0] + newUser.lastName).toLowerCase();
        },
        getAge(){
            let today = new Date();
            let age = today.getFullYear() - this.birthday.slice(-4);

            return age;
        },
        getPassword(){
            return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + this.birthday.slice(-4);
        },
    }
    newUser.login = newUser.getLogin();
    return newUser;
};
console.log(createNewUser().getLogin());
console.log(createNewUser().getAge());
console.log(createNewUser().getPassword());