"use strict";

let a = +prompt("First number");
while (!a || isNaN(a) || a === "") {
    a = +prompt("Number is not correct");
}

let b = +prompt("Second number");
while (!b || isNaN(b) || b === "") {
    b = +prompt("Number is not correct");
}

let operation = prompt("Enter operation");
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt("Operation is not correct");
}

function calc(num1, num2, oper) {
    if (oper === "+") {
        return num1 + num2;
    }else if (oper === "-") {
        return num1 - num2;
    }else if (oper === "*") {
        return num1 * num2;
    }else if (oper === "/") {
        return num1 / num2;
    }
}

console.log(calc(a, b, operation));